package com.qiyuesuo.login;

import java.io.Serializable;

public class CategoryDocumentResponse implements Serializable {

	private static final long serialVersionUID = 7567392688678147998L;

	private String fileName;
	private String fileKey;
	private String fileType;
	private PageInfoResponse pageInfo;

	private String categoryId;
	private String orgId;
	private String title;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileKey() {
		return fileKey;
	}

	public void setFileKey(String fileKey) {
		this.fileKey = fileKey;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public PageInfoResponse getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(PageInfoResponse pageInfo) {
		this.pageInfo = pageInfo;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
