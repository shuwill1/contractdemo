package com.qiyuesuo.login;

import java.io.Serializable;

public class DimensionType implements Serializable {

	private static final long serialVersionUID = 4633504036681333852L;

	private int width;
	private int height;
	private String type;

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
