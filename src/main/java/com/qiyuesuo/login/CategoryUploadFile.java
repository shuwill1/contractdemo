package com.qiyuesuo.login;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class CategoryUploadFile {

	public static void main(String[] args) throws IOException{
		// 登录获取Cookie
		final Header cookie = LoginApp.login(
				"https://privapp.qiyuesuo.me/login",
				"10000000001",
				"qiyuesuo#2020"
		);


		final CloseableHttpClient httpClient = HttpClients.createDefault();

		final HttpPost post = new HttpPost("https://privapp.qiyuesuo.me/category/checkparams");

		String categoryId = "2997449860796317840";
		final MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.addTextBody("categoryId", categoryId);

		File file = new File("/Users/wangshuwei/Downloads/example docs/测试PDF样式.docx");
		builder.addBinaryBody("file", file);

		final HttpEntity httpEntity = builder.build();
		post.setEntity(httpEntity);
		post.setHeader("cookie", cookie.getValue());

		final CloseableHttpResponse response = httpClient.execute(post);

		//解析response
		final StatusLine status = response.getStatusLine();
		if(status.getStatusCode() == 200) {
			System.out.println(status);
			HttpEntity responseEntity = response.getEntity();
			String result = EntityUtils.toString(responseEntity);
			System.out.println(result);

			final ObjectMapper objectMapper = new ObjectMapper();
			final CommonResponse<CategoryDocumentResponse> commonResponse = objectMapper.readValue(result,
					new TypeReference<CommonResponse<CategoryDocumentResponse>>(){});
			System.out.println(commonResponse.getCode());
			System.out.println(commonResponse.getResult().getFileKey());

			final String currentOrgId = getCurrentOrgId("https://privapp.qiyuesuo.me/user", cookie);
			addWord(
					"https://privapp.qiyuesuo.me/category/addword",
					cookie,
					commonResponse.getResult(),
					UUID.randomUUID().toString(),
					categoryId,
					currentOrgId
			);

		} else {
			System.out.println(status);
			throw new IOException("request fail");
		}
	}

	public static void addWord(
			String url,
			Header cookie,
			CategoryDocumentResponse document,
			String title,
			String categoryId,
			String orgId
	) throws IOException{

		document.setTitle(title);
		document.setCategoryId(categoryId);
		document.setOrgId(orgId);

		final CloseableHttpClient httpClient = HttpClients.createDefault();
		final HttpPost post = new HttpPost(url);

		ObjectMapper mapper =new ObjectMapper();
		final String requset = mapper.writeValueAsString(document);
		final StringEntity entity = new StringEntity(requset);
		post.setEntity(entity);

		post.setHeader("content-type", "application/json;charset=UTF-8");
		post.setHeader("cookie", cookie.getValue());

		final CloseableHttpResponse response = httpClient.execute(post);
		final StatusLine status = response.getStatusLine();
		if(status.getStatusCode() == 200) {
			final HttpEntity responseEntity = response.getEntity();
			System.out.println(EntityUtils.toString(responseEntity));
			System.out.println("添加文件成功");
		}
	}


	public static String getCurrentOrgId(String url, Header cookie) throws IOException{
		final CloseableHttpClient httpClient = HttpClients.createDefault();
		final HttpGet get = new HttpGet(url);
		get.setHeader("cookie", cookie.getValue());

		final CloseableHttpResponse response = httpClient.execute(get);
		final StatusLine status = response.getStatusLine();
		if(status.getStatusCode() == 200) {
			final HttpEntity responseEntity = response.getEntity();
			final String result = EntityUtils.toString(responseEntity);

			ObjectMapper objectMapper = new ObjectMapper();
			final CommonResponse<UserLoginInfoResponse> commonResponse = objectMapper.readValue(result, new TypeReference<CommonResponse<UserLoginInfoResponse>>() {
			});
			final UserLoginInfoResponse userLoginInfoResponse = commonResponse.getResult();
			return userLoginInfoResponse.getOrganization().getId();
		}else {
			System.out.println(status);
			throw new IOException("request fail");
		}
	}

}
