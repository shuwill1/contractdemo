package com.qiyuesuo.login;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class UserLoginInfoResponse implements Serializable {
	private static final long serialVersionUID = 7837054419946171814L;

	private OrganizationResponse organization;

	public OrganizationResponse getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationResponse organization) {
		this.organization = organization;
	}
}
