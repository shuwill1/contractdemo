package com.qiyuesuo.login;

import java.io.Serializable;

public class PageInfoResponse implements Serializable {

	private static final long serialVersionUID = 3914859564443902838L;

	private int pageCount;
	private DimensionType dimensionType;

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public DimensionType getDimensionType() {
		return dimensionType;
	}

	public void setDimensionType(DimensionType dimensionType) {
		this.dimensionType = dimensionType;
	}
}
