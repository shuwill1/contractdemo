package com.qiyuesuo.login;

import java.io.IOException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;


public class LoginApp {

	public static Header login(String url, String username, String password) throws IOException {
		final CloseableHttpClient httpClient = HttpClients.createDefault();
		final HttpPost post = new HttpPost(url);

		final MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.addTextBody("username", username);
		builder.addTextBody("password", password);
		final HttpEntity entity = builder.build();

		post.setEntity(entity);

		post.setHeader("X-Requested-With", "XMLHttpRequest");

		final CloseableHttpResponse response = httpClient.execute(post);
		final StatusLine status = response.getStatusLine();
		if(status.getStatusCode() == 200) {
			System.out.println("login to " + url + ", status: " + status);
			final Header[] headers = response.getHeaders("Set-Cookie");
			if(headers == null || headers.length < 1) {
				throw new IOException("未获取登陆Cookie");
			}
			final Header cookie = headers[0];
			System.out.println("login successful, the cookis is " + cookie);
			return cookie;
		} else {
			System.out.println(status);
			throw new IOException("登录失败");
		}

	}

}
